| Version | Status |
| ------ | ------ |
| **2019.8.15.0** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-kubectl/badges/2019.8.15.0/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-kubectl/commits/2019.8.15.0) |
| **2019.8.9.0** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-kubectl/badges/2019.8.9.0/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-kubectl/commits/2019.8.9.0) |
| **19-1.15.1** | [![pipeline status](http://svr-gitlab.grupocr.local/PI2K/docker/docker-kubectl/badges/19-1.15.1/pipeline.svg)](http://svr-gitlab.grupocr.local/PI2K/docker/docker-kubectl/commits/19-1.15.1) |

---

# docker-kubectl

Docker image with: Docker + Kubectl